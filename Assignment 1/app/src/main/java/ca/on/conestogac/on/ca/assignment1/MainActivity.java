package ca.on.conestogac.on.ca.assignment1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import org.w3c.dom.Text;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Button buttonOne;
    private Button buttonTwo;

    private TextView textViewOutcome;

    private ImageView imageViewUser;
    private ImageView imageViewComputer;

    private int userChoice;
    private int computerChoice;

    private static int getRandom (int a, int b) {
        return(
                b >= a
                        ? a + (int)Math.floor(Math.random() * (b - a + 1))
                        : getRandom(b, a)
        );
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonOne = findViewById(R.id.buttonOneFinger);
        buttonTwo = findViewById(R.id.buttonTwoFinger);
        textViewOutcome = findViewById(R.id.txtViewOutcome);
        imageViewUser = findViewById(R.id.imageViewUser);
        imageViewComputer = findViewById(R.id.imageViewComputer);


        buttonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewUser.setImageResource(R.drawable.ic_one);
                computerChoice = getRandom(1,2);
                userChoice = 1;

                if(userChoice == computerChoice){
                    imageViewComputer.setImageResource(R.drawable.ic_one);
                    textViewOutcome.setText("You win!");
                }
                else{
                    imageViewComputer.setImageResource(R.drawable.ic_two);
                    textViewOutcome.setText("The computer wins!");
                }
            }
        });
        buttonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewUser.setImageResource(R.drawable.ic_two);
                computerChoice = getRandom(1,2);
                userChoice = 2;
                if(userChoice == computerChoice){
                    imageViewComputer.setImageResource(R.drawable.ic_two);
                    textViewOutcome.setText("You win!");
                }
                else{
                    imageViewComputer.setImageResource(R.drawable.ic_one);
                    textViewOutcome.setText("The computer wins!");
                }

            }

        });

    }

}